set title
set ruler
set ic
set scrolloff=5
filetype off


" Neobundle
if has ('vim_starting')
    set runtimepath+=~/.vim/bundle/neobundle.vim/
endif

call neobundle#rc(expand('~/.vim/bundle/'))

NeoBundleFetch 'Shougo/neobundle.vim'

syntax on

" macでだけビルドすればいい
NeoBundle 'Shougo/vimproc.vim', {
      \ 'build' : {
      \     'mac' : 'make -f make_mac.mak',
      \     }
      \ }

" 全く使えていない
NeoBundle 'Shougo/neosnippet',{
      \ 'depends' : 'Shougo/neocomplcache'
      \ }
NeoBundle 'Shougo/neosnippet-snippets'

" なきゃ死ぬ
NeoBundle 'Shougo/unite.vim'

" ハイライト
NeoBundle 't9md/vim-quickhl'

" 使う必要性は感じてない
NeoBundle 'Shougo/vimshell.vim'

" ぜひ使いたいところ
NeoBundle 'Shougo/vimfiler.vim'
"NeoBundle 'tyru/skk.vim'

" なきゃ死ぬ
NeoBundle 'Shougo/neocomplcache'

" 入力補助
NeoBundleLazy 'Shougo/neocomplcache-rsense', {
      \ 'depends' : 'Shougo/neocomplcache',
      \ 'autoload' : { 'filetypes' : 'ruby' }
      \ }

" C#
NeoBundleLazy 'nosami/Omnisharp', {
      \ 'autoload' : { 'filetypes' : 'cs' }
      \ }

" haskellのインデント
NeoBundleLazy 'kana/vim-filetype-haskell', { 'autoload' : {
      \ 'filetypes' : 'haskell' }
      \ }

" haskellの補完
NeoBundleLazy 'ujihisa/neco-ghc', { 'autoload' : {
      \ 'filetypes' : 'haskell' }
      \ }

" haskellのhelpを引く
NeoBundleLazy 'ujihisa/ref-hoogle', { 'autoload' : {
      \ 'filetypes' : 'haskell'
      \ }}

" haskellのimportを便利に
NeoBundleLazy 'ujihisa/unite-haskellimport', { 'autoload' : {
      \ 'filetypes' : 'haskell'
      \ }}

" haskellのハイライト
NeoBundleLazy 'dag/vim2hs', { 'autoload' : {
      \ 'filetypes' : 'haskell'
      \ }}

" haskellのエラーチェック
NeoBundleLazy 'eagletmt/ghcmod-vim', { 'autoload' : {
      \ 'filetypes' : 'haskell'
      \ }}

" haddockを参照
NeoBundleLazy 'eagletmt/unite-haddock', { 'autoload' : {
      \ 'filetypes' : 'haskell',
      \   'depends' : 'Shougo/unite.vim',
      \ }}

" unite source
NeoBundle 'h1mesuke/unite-outline'

" helpひく
NeoBundle 'thinca/vim-ref'

" ys, cs, ds
NeoBundle 'tpope/vim-surround'

" syntax
NeoBundleLazy 'tpope/vim-rails', { 'autoload' : {
      \ 'filetypes' : 'ruby'
      \ }}

" syntax
NeoBundleLazy 'Keithbsmiley/rspec.vim', { 'autoload' : {
      \ 'filetypes' : 'rspec'
      \ }}

" ,ccでコメントアウト
NeoBundle 'vim-scripts/The-NERD-Commenter'

" 正規表現をスマートに書ける。sをSにするだけ。
NeoBundle 'othree/eregex.vim'

" なきゃ死ぬ
NeoBundleLazy 'mattn/emmet-vim', { 'autoload' : {
      \ 'filetypes' : ['html', 'css', 'scss', 'erb']
      \ }}

" syntax
NeoBundleLazy 'othree/html5.vim', { 'autoload' : {
      \ 'filetypes' : 'html'
      \ }}

" syntax
NeoBundleLazy 'hail2u/vim-css3-syntax', { 'autoload' : {
      \ 'filetypes' : 'css'
      \ }}

" なきゃ死ぬ
NeoBundleLazy 'tell-k/vim-browsereload-mac', { 'autoload' : {
      \ 'filetypes' : ['html', 'css', 'javascript','markdown','coffee','scss']
      \ }}

" syntax
NeoBundleLazy 'pangloss/vim-javascript', { 'autoload' : {
      \ 'filetypes' : 'javascript'
      \ }}

" jsの補完候補
NeoBundleLazy 'marijnh/tern_for_vim', { 'autoload' : {
      \ 'filetypes' : ['javascript','coffee']
      \ }}

" なきゃ死ぬ
NeoBundle 'thinca/vim-quickrun'

" jsのsyntax
NeoBundleLazy 'jelera/vim-javascript-syntax', {'autoload' : {
      \ 'filetypes' : 'javascript'
      \ }}

" 汎用syntax
NeoBundle 'scrooloose/syntastic'

" マークダウンのシンタックス
NeoBundleLazy 'tpope/vim-markdown',{ 'autoload' : {
      \ 'filetypes' : 'markdown'
      \ }}

" マークダウンをブラウザでプレビューするために
NeoBundle 'tyru/open-browser.vim'

" jsのローカル実行
NeoBundleLazy 'joyent/node', {'autoload' : {
      \ 'filetypes' : 'javascript'
      \ }}

" ディレクトリとべる
NeoBundle 'scrooloose/nerdtree'
NeoBundle 'jistr/vim-nerdtree-tabs'

" text objectを自前で拡張
NeoBundle 'kana/vim-textobj-user'

" rubyのキーワードをテキストオブジェクトにする
NeoBundleLazy 'rhysd/vim-textobj-ruby',{ 'autoload' : {
      \ 'filetypes' : 'ruby'
      \ }}

" requireの入力補助
NeoBundleLazy 'rhysd/unite-ruby-require.vim', {'autoload' : {
      \ 'filetypes' : 'ruby'
      \ }}

" git
NeoBundle 'tpope/vim-fugitive'

" syntax
NeoBundle 'vim-scripts/slimv.vim', {'autoload' : {
      \ 'filetypes' : 'lisp'
      \ }}

" なきゃ死ぬ
NeoBundle 'itchyny/lightline.vim'

" vimの変数を見やすくする
NeoBundleLazy 'thinca/vim-prettyprint', { 'autoload' : {
      \ 'filetypes' : 'vim',
      \ 'commands' : 'PP'
      \ }}

" まだ使ってない
"NeoBundleLazy 'rbtnn/vimconsole.vim', {
"      \ 'depends' : 'thinca/vim-prettyprint',
"      \ 'autoload' : {
"      \ 'commands' : 'VimConsoleOpen'
"      \ }}

" unite source
NeoBundleLazy 'kana/vim-tabpagecd', { 'autoload' : {
      \ 'unite_sources' : 'tab'
      \ }}

" .の拡張
NeoBundleLazy 'tpope/vim-repeat', { 'autoload' : {
      \ 'mappings' : '.'
      \ }}

" rubyのsyntax
NeoBundleLazy 'vim-ruby/vim-ruby', { 'autoload' : {
      \ 'filetypes' : 'ruby',
      \ }}

" unite source
NeoBundleLazy 'taka84u9/vim-ref-ri', { 'autoload' : {
      \ 'filetypes' : 'ruby',
      \ }}

" unite source
NeoBundleLazy 'Shougo/unite-help', { 'autoload' : {
      \ 'unite_sources' : 'help',
      \ }}

" unite source
NeoBundleLazy 'basyura/unite-rails', { 'autoload' : {
      \ 'filetypes' : 'ruby',
      \ }}

" coffeeのsyntax
NeoBundleLazy 'kchmck/vim-coffee-script', { 'autoload' : {
      \ 'filetypes' : 'coffee',
      \ }}

" hamlのsyntax
NeoBundleLazy 'tpope/vim-haml', { 'autoload' : {
      \ 'filetypes' : ['sass', 'haml', 'scss'],
      \ }}

" slimのsyntax
NeoBundleLazy 'slim-template/vim-slim', { 'autoload' : {
      \ 'filetypes' : 'slim'
      \ }}

" sassのシンタックス
NeoBundleLazy 'cakebaker/scss-syntax.vim',{ 'autoload' : {
      \ 'filetypes' : 'scss'
      \ }}

" if...endとか、対応するやつに%でとべる
NeoBundleLazy 'vim-scripts/ruby-matchit', { 'autoload' : {
      \ 'filetypes' : 'ruby',
      \ }}

" ifを打つとendを入れてくれる
NeoBundleLazy 'tpope/vim-endwise', { 'autoload' : {
      \ 'filetypes' : ['ruby', 'c', 'lua','vim'],
      \ }}

" yank履歴を見れるようにする
NeoBundle 'vim-scripts/YankRing.vim'

" undo履歴を見れるようにする
NeoBundle 'mbbill/undotree'

" bufferを一覧する
NeoBundle 'troydm/easybuffer.vim'

" 一文字の入力を器用に設定する
NeoBundle 'kana/vim-smartchr'

" vimrcのベンチマーク
NeoBundleLazy 'mattn/benchvimrc-vim', { 'autoload' : {
      \   'filetypes' : 'vim'
      \ }}

" 登録した語句を切り替える。使いこなせれば便利だろう
" 全部の言語で使いそう
NeoBundle 'AndrewRadev/switch.vim'

" コメント関係
NeoBundle 'tomtom/tcomment_vim'

" はてなブログ用
NeoBundle 'mattn/webapi-vim'
NeoBundleLazy 'moznion/hateblo.vim', { 'autoload' : {
      \   'filetypes' : 'text',
      \   'depends': ['mattn/webapi-vim', 'Shougo/unite.vim']
      \ }}


filetype plugin indent on

" Installation check
NeoBundleCheck

syntax enable


"---------------------------------------------------------------------
" vimの設定
"---------------------------------------------------------------------

set nocompatible " vi互換を切って、vimデフォルトの動きにする

set number " 行番号を表示する

set hidden " 保存されてないファイルがあっても別のファイルを開ける
set vb t_vb= " ビープ音じゃなくて、ビジュアルベルを使う

set autoread " vimの外でファイルが変更された時に自動で読み込む

" window設定
set splitbelow " splitで下に出す
set splitright " vsplitで右に出す

" インデント
set autoindent " 改行時に、カレント行のインデントをコピー
set smartindent " プログラミング言語を自動インデント


" 検索
set hlsearch " ハイライト
set incsearch " インクリメンタルサーチ
set ignorecase " 大文字小文字を区別しない
set smartcase " 大文字と小文字が混ざった時だけ区別する
set nowrapscan " ファイルの最後まで検索したら上に戻らない

" カッコ関連
set showmatch " 対応するカッコが光る

" モード関連
set showmode " 現在のモードを表示

" スペース、タブ関連
set tabstop=2 " tabをスペース2つに設定
set softtabstop=2 " tabを押すとスペース2つを入力
set shiftwidth=2 " インデントをスペース2つに設定
set expandtab " tab、<、>、入力時にインデントをする
set smarttab " BS時にshiftwidth分のスペースを消す

" エンコード
set encoding=utf-8 " vim内で使うエンコード
set fileencodings=utf8 " 既存のファイルを読むエンコード
set termencoding=utf-8 " ターミナルのエンコード

set whichwrap=b,s,h,l,<,>,[,] " カーソル移動
set backspace=indent,eol,start " BSをどこでも使えるように
set browsedir=buffer " ディレクトリもvimで開く

" ステータスライン
set laststatus=2 " ステータス行を常に表示する
set statusline=%<%f\ %m%r%h%w%{'['.(&fenc!=''?&fenc:&enc).']['.&ff.']'}%=%l,%c%V%8P " ステータスラインの内容を設定

set showcmd

set wildmode=list,full " 補完の候補をリスト表示
set wildmenu " コマンドモードで補完の候補を表示

set virtualedit=all " 仮想編集。文字のない場所にもカーソルが当たる

set modelines " 編集するファイル内にvimの設定を書ける

set clipboard=unnamed,autoselect

if exists('&ambiwidth')
  set ambiwidth=double
endif

highlight Pmenu ctermbg=4

"swp files
set directory-=.

"backup files
set backup
set backupdir=/tmp

"show special character
set list
set lcs=tab:>-,trail:_,extends:>,precedes:<,nbsp:x

"scriptencoding utf-8
"augroup highlightIdegraphicSpace
    "autocmd!
    "autocmd ColorScheme * highlight IdeographicSpace term=underline ctermbg=DarkGreen guibg=DarkGreen
    "autocmd VimEnter,WinEnter * match IdeographicSpace /　/
"augroup END

"colorscheme desert-warm-256

" remove autocomment
autocmd FileType * set formatoptions-=ro


"---------------------------------------------------------------------
" Key mappings
"---------------------------------------------------------------------

" mapleader
let mapleader = ","

" nomal mode basics
nnoremap <Esc><Esc> :<C-u>noh<CR>
nnoremap <Space>j G
nnoremap <Space>k gg
nnoremap <Space>a *
nnoremap <Space>p %
nnoremap <Space>w :<C-u>write<CR>
nnoremap <Space>v :<C-u>tabnew ~/.vimrc<CR>
nnoremap <Space>s :<C-u>source ~/.vimrc<CR>
nnoremap ; :
nnoremap : ;
nnoremap <Space>c :<C-u>ChromeReload<CR>

nnoremap <Space>g :<C-u>tabnew ~/.gvimrc<CR>

nnoremap <Space>zz :<C-u>tabnew ~/.zshrc<CR>
nnoremap <Space>ze :<C-u>tabnew ~/.zshenv<CR>
nnoremap <Space>za :<C-u>tabnew ~/.zshalias<CR>

nnoremap <Space>zlz :<C-u>tabnew ~/.zshrc.local<CR>
nnoremap <Space>zle :<C-u>tabnew ~/.zshenv.local<CR>
nnoremap <Space>zla :<C-u>tabnew ~/.zshalias.local<CR>

nnoremap <Space>e :<C-u>e ++enc=utf-8<CR> :<C-u>set encoding=utf-8<CR> :<C-u>w<CR>

nnoremap <Space>] <C-w>]
nnoremap E $a<Space>
nnoremap <C-x> :<C-u>tabclose<CR>
nnoremap <C-t> :<C-u>tabnew<CR>


" insert mode basics
" どこでもつかうやつ
inoremap <C-c> <Esc>
inoremap }} {}<Left><CR><ESC>O
inoremap { {}<Left>
inoremap )) ()<Left><CR><ESC>O
inoremap [ []<Left>
inoremap ( ()<Left>
inoremap <> <><Left>
inoremap '' ''<Left>
inoremap " ""<Left>
inoremap <C-f> %
inoremap <C-d> $
inoremap <C-a> @
inoremap <C-w> &
inoremap <C-b> \
inoremap <C-p> +
inoremap <Tab> <C-x><C-o>


" rubyで||を打ったらカーソルを戻す
autocmd FileType ruby,slim inoremap \|\| \|\|<Left>


" 比較演算子をperl風に書けるように
autocmd FileType ruby,coffee,python,vim inoreabbrev gt >
autocmd FileType ruby,coffee,python,vim inoreabbrev ge >=
autocmd FileType ruby,coffee,python,vim inoreabbrev lt <
autocmd FileType ruby,coffee,python,vim inoreabbrev le <=
autocmd FileType ruby,coffee,python,vim inoreabbrev eq ==
autocmd FileType ruby,coffee,python,vim inoreabbrev ne !=


" 代入演算子が打ちやすいように
inoreabbrev inc +=
inoreabbrev dec -=
inoreabbrev aeo *=
inoreabbrev seo /=
inoreabbrev peo %=

" nil guard
autocmd Filetype ruby inoreabbrev nilg \|\|=

" lambda
autocmd Filetype ruby inoreabbrev la ->

" jsではこっち
autocmd FileType javascript inoreabbrev eq ===
autocmd FileType javascript inoreabbrev ne !==

" andとorも
autocmd FileType javascript,vim inoreabbrev and &&
autocmd FileType javascript,vim inoreabbrev or \|\|

" UFO演算子はRubyだけでいいかな
autocmd FileType ruby inoreabbrev cmp <=>

" 正規表現の演算子
autocmd FileType ruby inoreabbrev ma =~

" emmetがあるやつはtabでemmet展開
autocmd FileType html,css,erb,scss imap <Tab> <plug>(EmmetExpandAbbr)

" coffeescript用
autocmd FileType coffee inoreabbrev fc ->

" for command mode
cnoremap <C-h> <BS>
cnoremap <C-f> <Right>
cnoremap <C-t> ~
cnoremap <C-b> <Left>

" for visual mode
vnoremap ; :
vnoremap : ;

" for python
autocmd BufWritePre * :%s/\s\+$//ge
" setlocal textwidth=80

" for common lisp"
map <Leader>r <ESC>:!clisp -i %<CR>

" for others
vnoremap < <gv
vnoremap > >gv


" expand path
cmap <C-x> <C-r>=expand('%:p:h')<CR>/
" expand file (not ext)
cmap <C-z> <C-r>=expand('%:p:r')<CR>

" ascii
nnoremap <Leader>a <Esc>:ascii<CR>

" add \n
nnoremap U :<C-u>call append(expand('.'), '')<Cr>j

" replace Y
nnoremap Y y$


" repeat command
nnoremap c. q:k<Cr>

" no search offset
cnoremap <expr> /  getcmdtype() == '/' ? '\/' : '/'
cnoremap <expr> ?  getcmdtype() == '?' ? '\?' : '?'

" substitute word under cursor
nnoremap <expr> s* ':%substitute/\<' . expand('<cword>') . '\>/'

" replace word under cursor with yanked string
nnoremap <silent> ciy ciw<C-r>0<ESC>:let@/=@1<CR>:noh<CR>
nnoremap <silent> cy   ce<C-r>0<ESC>:let@/=@1<CR>:noh<CR>

" tabnew with gf
nnoremap gf <C-w>gf


"---------------------------------------------------------------------
" templates
"---------------------------------------------------------------------

"---------------------------------------------------------------------
" Key mappings for vim windows
"---------------------------------------------------------------------

nnoremap sj <C-W>j
nnoremap sk <C-W>k
nnoremap sh <C-W>h
nnoremap sl <C-W>l
nnoremap so <C-W>o
nnoremap ss <C-W>s
nnoremap sc <C-W>c
nnoremap so <C-W>o

nnoremap + <C-W>+
nnoremap - <C-W>-

nnoremap ) <C-W>>
nnoremap ( <C-W><LT>

function! s:big()
  wincmd _ | wincmd |
endfunction
nnoremap <silent> s<CR> :<C-u>call <SID>big()<CR>
nnoremap s0 1<C-W>_
nnoremap s. <C-W>=


"---------------------------------------------------------------------
" settings for filetypes
"---------------------------------------------------------------------

autocmd BufNewFile,BufRead *.scala set filetype=scala
autocmd BufNewFile,BufRead *.tt set filetype=html
autocmd BufNewFile,BufRead *.mt set filetype=html
autocmd BufNewFile,BufRead *.tx set filetype=html
autocmd BufNewFile,BufRead *.page set filetype=markdown
autocmd BufNewFile,BufRead *.m set filetype=objc
autocmd BufNewFile,BufRead *_spec.rb set filetype=rspec
autocmd BufNewFile,BufRead *.coffee set filetype=coffee
autocmd BufNewFile,BufRead *.sass set filetype=sass
autocmd BufNewFile,BufRead *.scss set filetype=scss
autocmd BufNewFile,BufRead *.haml set filetype=haml
autocmd BufNewFile,BufRead *.slim set filetype=slim


"---------------------------------------------------------------------
" settings for tabs
"---------------------------------------------------------------------

nnoremap <C-h> gT
nnoremap <C-l> gt

set showtabline=2
set tabline=%!MakeTabLine()

function! MakeTabLine()
    let titles = map(range(1, tabpagenr('$')), 's:tabpage_label(v:val)')
    let tabpages = join(titles) . ' ' . '%#TabLineFill#%T'
    let info = fnamemodify(getcwd(), ":~") . ' '
    return tabpages . '%=' . info
endfunction

function! s:tabpage_label(n)
    let bufnrs = tabpagebuflist(a:n)

    let hi = a:n is tabpagenr() ? '%#TabLineSel#' : '%#TabLine#'

    let no = len(bufnrs)
    if no is 1
        let no = ''
    endif

    let mod = len(filter(copy(bufnrs), 'getbufvar(v:val, "&modified")')) ? '+' : ''
    let sp = (no . mod) ==# '' ? '' : ' '

    let curbufnr = bufnrs[tabpagewinnr(a:n) - 1]
    let fname = pathshorten(bufname(curbufnr))

    let label = ' ' . no . mod . sp . fname

    return '%' . a:n . 'T' . hi . label
endfunction



hi TabLine term=reverse cterm=reverse ctermfg=white ctermbg=black
hi TabLineSel term=bold cterm=bold,underline ctermfg=6
hi TabLineFill term=reverse cterm=reverse ctermfg=white ctermbg=black



"---------------------------------------------------------------------
" for quickhl
"---------------------------------------------------------------------

let b:surround_123 = "+{ \r }"

nmap <Space>m <Plug>(quickhl-toggle)
xmap <Space>m <Plug>(quickhl-toggle)
nmap <Space>M <Plug>(quickhl-reset)
xmap <Space>M <Plug>(quickhl-reset)


"---------------------------------------------------------------------
" for surround.vim
"---------------------------------------------------------------------

let mapleader=","
" double quatation
nmap <leader>q <Plug>Csurround w"
imap <leader>q <ESC><Plug>Csurround w"<Right>wa

" single quatation
nmap <leader>sq <Plug>Csurround w'
imap <leader>sq <ESC><Plug>Csurround w'<Right>wa

" parenthesis
nmap <leader>p <Plug>Csurround w)
imap <leader>p <ESC><Plug>Csurround w)<Right>wa

" square bracket
nmap <leader>sb <Plug>Csurround w]
imap <leader>sb <ESC><Plug>Csurround w]<Right>wa


"---------------------------------------------------------------------
" for neocomplcache.vim
"---------------------------------------------------------------------

" Use neocomplcache.
let g:neocomplcache_enable_at_startup = 1
" Use underbar completion.
let g:neocomplcache_enable_underbar_completion = 1
" Set minimum syntax keyword length.
let g:neocomplcache_min_syntax_length = 3
" 自動でロックするバッファ名のパターンを指定
let g:neocomplcache_lock_buffer_name_pattern = '\*ku\*'

" ディレクトリ指定
let g:neocomplcache_temporary_dir = "$HOME/.vim/tmp/neocomplcache"

let g:neocomplcache_ctags_arguments_list = {
  \ 'perl' : '-R -h ".pm"'
  \ }

" Define dictionary.
let g:neocomplcache_dictionary_filetype_lists = {
    \ 'default' : '',
    \ 'perl' : $HOME . '/.vim/dict/perl.dict',
    \ 'javascript' : $HOME . '/.vim/dict/javascript.dict',
    \ 'html' : $HOME . '/.vim/dict/javascript.dict',
    \ 'scala' : $HOME . '/.vim/dict/scala.dict',
    \ 'ruby' : $HOME . '.vim/dict/ruby.dict',
    \ 'python' : $HOME . '.vim/dict/python.dict',
    \ 'objc' : $HOME . '.vim/dict/objc.dict',
    \ }

" Define keyword.
if !exists('g:neocomplcache_keyword_patterns')
  let g:neocomplcache_keyword_patterns = {}
endif
let g:neocomplcache_keyword_patterns['default'] = '\h\w*'

" for snippets
imap <expr><C-k> neocomplcache#sources#snippets_complete#expandable() ? "\<Plug>(neocomplcache_snippets_expand)" : "\<C-k>"
smap <C-k> <Plug>(neocomplcache_snippets_expand)
let g:neocomplcache_snippets_dir = "~/.vim/snippets"

" Plugin key-mappings.
inoremap <expr><C-g>     neocomplcache#undo_completion()
inoremap <expr><C-l>     neocomplcache#complete_common_string()

" Recommended key-mappings.
" <C-h>, <BS>: close popup and delete backword char.
inoremap <expr><C-h> pumvisible() ? neocomplcache#close_popup()."\<C-h>" : "\<C-h>"
inoremap <expr><BS> pumvisible() ? neocomplcache#close_popup()."\<C-h>" : "\<C-h>"
inoremap <expr><C-y>  neocomplcache#close_popup()
inoremap <expr><C-e>  neocomplcache#cancel_popup()

" AutoComplPop like behavior.
let g:neocomplcache_enable_auto_select = 1
inoremap <expr><C-h> pumvisible() ? neocomplcache#cancel_popup()."\<C-h>" : "\<C-h>"
inoremap <expr><BS> pumvisible() ? neocomplcache#cancel_popup()."\<C-h>" : "\<C-h>"

" Enable omni completion.
autocmd FileType css setlocal omnifunc=csscomplete#CompleteCSS
autocmd FileType html,markdown setlocal omnifunc=htmlcomplete#CompleteTags
autocmd FileType javascript,coffee setlocal omnifunc=javascriptcomplete#CompleteJS
autocmd FileType python setlocal omnifunc=pythoncomplete#Complete
autocmd FileType ruby setlocal omnifunc=rubycomplete#Complete
autocmd FileType xml setlocal omnifunc=xmlcomplete#CompleteTags
autocmd FileType haskell setlocal omnifunc=necoghc#omnifunc

" Enable heavy omni completion.
if !exists('g:neocomplcache_omni_patterns')
	let g:neocomplcache_omni_patterns = {}
endif
let g:neocomplcache_omni_patterns.ruby = '[^. *\t]\.\w*\|\h\w*::'

" jsの補完でjscomplete使う
let g:neocomplcache_source_rank = {
      \ 'jscomplete' : 500,
      \ }

" domを入れる
let g:jscomplete_use = ['dom']

"---------------------------------------------------------------------
" home directory with \
"---------------------------------------------------------------------

function! HomedirOrBackslash()
  if getcmdtype() == ':' && (getcmdline() =~# '^e ' || getcmdline() =~? '^r\?!' || getcmdline() =~? '^cd ' || getcmdline() =~? '^tabnew ' || getcmdline() =~? '^source ')
    return '~/'
  else
    return '\'
  endif
endfunction
cnoremap <expr> <Bslash> HomedirOrBackslash()


"---------------------------------------------------------------------
" Save fold settings.
"---------------------------------------------------------------------

" autocmd BufWritePost * if expand('%') != '' && &buftype !~ 'nofile' | mkview | endif
" autocmd BufRead * if expand('%') != '' && &buftype !~ 'nofile' | silent loadview | endif
" Don't save options.
set viewoptions-=options


"---------------------------------------------------------------------
" for NeoBundle
"---------------------------------------------------------------------

nnoremap <Space>bi :<C-u>NeoBundleInstall<CR>
nnoremap <Space>bc :<C-u>NeoBundleClean<CR>
nnoremap <Space>bu :<C-u>NeoBundleUpdate<CR>


"---------------------------------------------------------------------
" for quickrun
"---------------------------------------------------------------------

nnoremap <Space>q :<C-u>QuickRun<CR>

let g:quickrun_config = {
      \ "_" : {
      \   "runner": "vimproc",
      \   "runner/vimproc/updatetime": 10,
      \}
      \}

let g:quickrun_config['cs'] = {
  \ 'command' : 'mcs',
  \ 'runmode' : 'simple',
  \ 'exec' : ['%c %s > /dev/null', 'mono "%S:p:r:gs?/?\\?.exe" %a', ':call delete("%S:p:r.exe")'],
  \ 'tempfile' : '{template()}.cs',
  \}

" for coffee script
" コンパイル結果のjsを出す
"let g:quickrun_config['coffee'] = {
"  \ 'command' : 'coffee',
"  \ 'exec' : ['%c -cbp %s']
"  \ }

" マークダウンをブラウザでプレビューする
let g:quickrun_config = {}
let g:quickrun_config['markdown'] = {
      \    'outputter': 'browser'
      \ }


"---------------------------------------------------------------------
" for unite.vim
"---------------------------------------------------------------------

nnoremap [unite] <Nop>
xnoremap [unite] <Nop>
nmap <Space>u [unite]
xmap <Space>u [unite]

nnoremap [unite]f :<C-u>Unite file<CR>
nnoremap [unite]b :<C-u>Unite buffer<CR>
nnoremap [unite]m :<C-u>Unite file_mru<CR>
nnoremap [unite]rr :<C-u>Unite ruby/require<CR>
nnoremap [unite]ri :<C-u>Unite ref/ri<CR>
nnoremap [unite]n :<C-u>Unite file/new<CR>
nnoremap [unite]dn :<C-u>Unite directory/new<CR>
nnoremap [unite]dm :<C-u>Unite directory_mru<CR>
nnoremap [unite]h :<C-u>Unite history/yank<CR>
nnoremap [unite]c :<C-u>Unite change<CR>
nnoremap [unite]t :<C-u>Unite tab<CR>
nnoremap [unite]km :<C-u>Unite mapping<CR>
nnoremap [unite]s :<C-u>Unite -buffer-name=search line -start-insert<CR>
nnoremap [unite]ms :<C-u>Unite menu:shortcut<CR>
nnoremap [unite]mo :<C-u>Unite rails/model<CR>
nnoremap [unite]ct :<C-u>Unite rails/controller<CR>
nnoremap [unite]vi :<C-u>Unite rails/view<CR>
nnoremap [unite]he :<C-u>Unite rails/helper<CR>
nnoremap [unite]li :<C-u>Unite rails/lib<CR>
nnoremap [unite]db :<C-u>Unite rails/db<CR>
nnoremap [unite]cf :<C-u>Unite rails/config<CR>
nnoremap [unite]js :<C-u>Unite rails/javascript<CR>
nnoremap [unite]cs :<C-u>Unite rails/stylesheet<CR>
nnoremap [unite]ge :<C-u>Unite rails/bundled_gem<CR>
nnoremap [unite]ro :<C-u>Unite rails/route<CR>
nnoremap [unite]rt :<C-u>Unite rails/root<CR>


" ウィンドウを分割して開く
au FileType unite nnoremap <silent> <buffer> <expr> <C-j> unite#do_action('split')
au FileType unite inoremap <silent> <buffer> <expr> <C-j> unite#do_action('split')

au FileType unite nnoremap <silent> <buffer> <expr> <C-l> unite#do_action('vsplit')
au FileType unite inoremap <silent> <buffer> <expr> <C-l> unite#do_action('vsplit')

" /をuniteで検索するようにする
" nnoremap <silent> / :<C-u>Unite -buffer-name=search line/fast -start-insert -no-quit<CR>

" menu:shortcutの一覧
let g:unite_source_menu_menus = {
      \  'shortcut' : {
      \    'description' : 'shortcuts',
      \    'command_candidates' : [
      \      ['edit vimrc', 'tabnew ~/.vimrc'],
      \      ['edit gvimrc', 'tabnew ~/.gvimrc'],
      \      ['recent open file', 'Unite file_mru'],
      \      ['open file', 'Unite file'],
      \      ['git status', 'Gstatus'],
      \      ['git add', 'Gwrite'],
      \      ['git commit', 'Gcommit'],
      \      ['git diff', 'Gdiff'],
      \      ['git log', 'Glog'],
      \      ['git push', 'Git push'],
      \      ['read ri', 'Unite ref/ri'],
      \      ['[Rails]open model', 'Unite rails/model'],
      \      ['[Rails]open controller', 'Unite rails/controller'],
      \      ['[Rails]open view', 'Unite rails/view'],
      \      ['[Rails]open helper', 'Unite rails/helper'],
      \      ['[Rails]open lib', 'Unite rails/lib'],
      \      ['[Rails]open database', 'Unite rails/db'],
      \      ['[Rails]open config', 'Unite rails/config'],
      \      ['[Rails]open javascript', 'Unite rails/javascript'],
      \      ['[Rails]open css', 'Unite rails/stylesheet'],
      \      ['[Rails]open gemfile', 'Unite rails/bundled_gem'],
      \      ['[Rails]open route', 'Unite rails/route'],
      \      ['[Rails]open root', 'Unite rails/root'],
      \     ],
      \  },
      \}

" デフォルトでタブで開く
call unite#custom#default_action('file, buffer', 'tabopen')


"---------------------------------------------------------------------
" detect char code
"---------------------------------------------------------------------

"if &encoding !=# 'utf-8'
"  set encoding=japan
"  set fileencoding=japan
"endif
"if has('iconv')
"  let s:enc_euc = 'euc-jp'
"  let s:enc_jis = 'iso-2022-jp'
"  if iconv('\x87\x64\x87\x6a', 'cp932', 'eucjp-ms') ==# '\xad\xc5\xad\xcb'
"    let s:enc_euc = 'eucjp-ms'
"    let s:enc_jis = 'iso-2022-jp-3'
"  elseif iconv("\x87\x64\x87\x6a", 'cp932', 'euc-jisx0213') ==# '\xad\xc5\xad\xcb'
"    let s:enc_euc = 'euc-jisx0213'
"    let s:enc_jis = 'iso-2022-jp-3'
"  endif
"  if &encoding ==# 'utf-8'
"    let s:fileencodings_default = &fileencodings
"	let &fileencodings = s:enc_jis .','. s:enc_euc .',cp932'
"    let &fileencodings = &fileencodings .','. s:fileencodings_default
"    unlet s:fileencodings_default
"  else
"    let &fileencodings = &fileencodings .','. s:enc_jis
"    set fileencodings+=utf-8,ucs-2le,ucs-2
"    if &encoding =~# '^\(euc-jp\|euc-jisx0213\|eucjp-ms\)$'
"      set fileencodings+=cp932
"      set fileencodings-=euc-jp
"      set fileencodings-=euc-jisx0213
"      set fileencodings-=eucjp-ms
"      let &encoding = s:enc_euc
"      let &fileencoding = s:enc_euc
"  else
"      let &fileencodings = &fileencodings .','. s:enc_euc
"    endif
"  endif
"  unlet s:enc_euc
"  unlet s:enc_jis
"endif
"if has('autocmd')
"  function! AU_ReCheck_FENC()
"    if &fileencoding =~# 'iso-2022-jp' && search("[^\x01-\x7e]", 'n') == 0
"      let &fileencoding=&encoding
"    endif
"  endfunction
"  autocmd BufReadPost * call AU_ReCheck_FENC()
"endif
"
"
"if filereadable(expand('~/.vimrc.local'))
"  source ~/.vimrc.local
"endif


"---------------------------------------------------------------------
" for browereload-mac \
"---------------------------------------------------------------------

nnoremap <Space>sta :<C-u>ChromeReloadStart<CR>
nnoremap <Space>sto :<C-u>ChromeReloadStop<CR>

"open file in Chrome
nnoremap <Space>oc :<C-u>!open -a Google\ Chrome %<CR><CR>

"save action
let g:returnApp = "iTerm"


"---------------------------------------------------------------------
" for vimshell
"---------------------------------------------------------------------

nnoremap ,is :<C-u>VimShell<CR>
nnoremap ,ipy :<C-u>VimShellInteractive python<CR>
nnoremap ,irb :<C-u>VimShellInteractive irb<CR>


"---------------------------------------------------------------------
" for NERDTree
"---------------------------------------------------------------------

nnoremap <Space>n :<C-u>NERDTreeTabsToggle<CR>


"---------------------------------------------------------------------
" for rspec.vim
"---------------------------------------------------------------------

autocmd BufReadPost,BufNewFile *_foo.rb set syntax=rspec


"---------------------------------------------------------------------
" for lisp
"---------------------------------------------------------------------

let g:paredit_mode=1
let g:paredit_electric_return = 0

function! s:generate_lisp_tags()
  let g:slimv_ctags = 'ctags -a -f '.$HOME.'/.vim/tags/lisp.tags'.expand('%:p').' --language-force=Lisp'
  call SlimvGenerateTags()
endfunction
command! -nargs=0 GenerateLispTags call <SID>generate_lisp_tags()

function! s:generate_lisp_tags_recursive()
  let g:slimv_ctags = 'ctags -a -f '.$HOME.'/.vim/tags/lisp.tags -R '.expand('%:p:h').' --language-force=Lisp'
  call SlimvGenerateTags()
endfunction
command! -nargs=0 GenerateLispTagsRecursive call <SID>generate_lisp_tags_recursive()

let g:slimv_repl_split = 4
let g:slimv_repl_name = 'REPL'
let g:slimv_repl_simple_eval = 0
let g:slimv_lisp = '/usr/local/bin/clisp'
let g:slimv_impl = 'clisp'
let g:slimv_preferred = 'clisp'
let g:slimv_swank_cmd = '!osascript -e "tell application \"Terminal\" to do script \"clisp '.$HOME.'/.vim/bundle/slimv/slime/start-swank.lisp\""'

let g:lisp_ranbow=1

autocmd BufNewFile,BufRead *.asd  set filetype=lisp


"---------------------------------------------------------------------
" for unite-ruby-require.vim
"---------------------------------------------------------------------

let g:unite_source_ruby_require_cmd = '$HOME/.rbenv/shims/ruby'


"---------------------------------------------------------------------
" for fugitive
"---------------------------------------------------------------------

nnoremap [git] <Nop>
xnoremap [git] <Nop>
nmap <Space>g [git]
xmap <Space>g [git]

nnoremap [git]st :<C-u>Gstatus<CR>
nnoremap [git]ad :<C-u>Gwrite<CR>
nnoremap [git]ci :<C-u>Gcommit<CR>
nnoremap [git]lg :<C-u>Glog<CR>
nnoremap [git]df :<C-u>Gdiff<CR>
nnoremap [git]ps :<C-u>Git push<CR>


"---------------------------------------------------------------------
" for vim-coffee-script
"---------------------------------------------------------------------

"taglistの設定
let g:tlist_coffee_settings = 'coffee;f:function;v:variable'

"保存時にコンパイルする
""autocmd BufWritePost *.coffee silent make -c


"---------------------------------------------------------------------
" for switch.vim
"---------------------------------------------------------------------

" !でswitchを動かす
nnoremap ! :Switch<CR>

" rubyのときのswitch
autocmd FileType ruby let b:switch_custom_definitions =
      \ [
      \   ['if', 'unless'],
      \   ['include', 'extend'],
      \   ['while', 'until'],
      \   ['class', 'module'],
      \   ['any?', 'all?'],
      \   ['attr_accessor', 'attr_reader', 'attr_writer'],
      \   ['first', 'last'],
      \   ['push', 'pop'],
      \   ['shift', 'unshift'],
      \   ['index', 'rindex'],
      \   ['has_key?', 'has_value?'],
      \   ['one?', 'none?'],
      \   ['protected_methods', 'public_methods', 'private_methods'],
      \   ['instance_variables_get', 'instance_variables_set'],
      \   ['upto', 'downto'],
      \   ['upcase', 'downcase'],
      \   ['start_with?', 'end_with?'],
      \   ['partition', 'rpartition'],
      \   ['eval', 'instance_eval'],
      \   ['send', 'public_send'],
      \   ['true', 'false'],
      \   ['and', 'or'],
      \   ['<', '>', '<=', '>='],
      \   ['odd?', 'even?'],
      \   ['chr', 'ord'],
      \   ['floor', 'ceil', 'round'],
      \ ]

" javascriptのときのswitch
autocmd FileType javascript let b:switch_custom_definitions =
      \ [
      \   ['&&', '||'],
      \   ['++', '--'],
      \   ['===', '!=='],
      \   ['true', 'false'],
      \   ['addClass', 'removeClass', 'toggleClass'],
      \   ['push', 'pop'],
      \   ['bind', 'unbind'],
      \   ['window', 'document'],
      \   ['before', 'after'],
      \   ['ajaxStart', 'ajaxStop'],
      \   ['ajaxError', 'ajaxSuccess'],
      \   ['fire', 'fired', 'fireWith'],
      \   ['pageX', 'pageY'],
      \   ['stopImmediatePropagetion', 'stopPropagation', 'preventDefault'],
      \   ['fadeIn', 'fadeOut', 'fadeToggle'],
      \   ['focusIn', 'focusOut'],
      \   ['mouseenter', 'mouseleave', 'hover'],
      \   ['innerHTML', 'outerHTML'],
      \   ['innerWidth', 'innerHeight'],
      \   ['outerWidth', 'outerHeight'],
      \   ['nth-child', 'nth-last-child'],
      \   ['on', 'off'],
      \   ['parseJSON', 'parseHTML', 'parseXML'],
      \   ['keydown', 'keypress', 'keyup'],
      \   ['attr', 'removeAttr'],
      \   ['scroll', 'scrollLeft', 'scrollTop'],
      \   ['hide', 'show'],
      \   ['slideDown', 'slideUp', 'slideToggle'],
      \ ]

" cssのswitch
autocmd FileType css let b:switch_custom_definitions =
      \ [
      \   ['border-top', 'border-bottom', 'border-left', 'border-right'],
      \   ['border-top-width', 'border-bottom-width', 'border-left-width', 'border-right-width'],
      \   ['border-top-style', 'border-bottom-style', 'border-left-style', 'border-right-style'],
      \   ['margin-top', 'margin-bottom', 'margin-left', 'margin-right'],
      \   ['padding-top', 'padding-bottom', 'padding-left', 'padding-right'],
      \   ['margin', 'padding'],
      \   ['min-width', 'max-width'],
      \   ['min-height', 'max-height'],
      \   ['height', 'width'],
      \   ['absolute', 'relative', 'fixed'],
      \   ['none', 'block'],
      \   ['overflow', 'overflow-x', 'overflow-y'],
      \   ['left', 'right', 'top', 'bottom'],
      \   ['before', 'after'],
      \   ['transition', 'animation'],
      \   ['em', 'px', '%'],
      \   ['white', 'black', 'gray'],
      \ ]


"---------------------------------------------------------------------
" for syntastic
"---------------------------------------------------------------------

"let g:syntastic_ruby_checkers = ['rubocop']
"let g:syntastic_quiet_warnings = 0


"---------------------------------------------------------------------
" for vim-rails
"---------------------------------------------------------------------

autocmd User Rails.view* NeoSnippetSource ~/.vim/snippet/ruby.rails.view.snip
autocmd User Rails.controller* NeoSnippetSource ~/.vim/snippet/ruby.rails.controller.snip
autocmd User Rails/db/migrate/* NeoSnippetSource ~/.vim/snippet/ruby.rails.migrate.snip
autocmd User Rails/config/routes.rb NeoSnippetSource ~/.vim/snippet/ruby.rails.routes.snip

let g:rails_level=4
let g:rails_default_file='app/controllers/application.rb'
let g:rails_default_database='sqlite3'


"---------------------------------------------------------------------
" for YankRing
"---------------------------------------------------------------------

" yank履歴を見れるようにする
nnoremap <C-y> :YRShow<CR>


"---------------------------------------------------------------------
" for undotree
"---------------------------------------------------------------------

" undo履歴を見れるようにする
nnoremap <C-u> :<C-u>UndotreeToggle<CR>

" undotreeの設定
let g:undotree_SetFocusWhenToggle = 1
let g:undotree_WindowLayout = 'topleft'
let g:undotree_SplitWidth = 35
let g:undotree_DiffAutoOpen = 1
let g:undotree_RelativeTimestamp = 1
let g:undotree_TreeNodeShape = "*"
let g:undotree_HighlightChangedText = 1


"---------------------------------------------------------------------
" for easybuffer
"---------------------------------------------------------------------

" easybufferのショートカットを設定
nnoremap <C-b> :<C-u>EasyBuffer<CR>


"---------------------------------------------------------------------
" for smartchr
"---------------------------------------------------------------------

" ,を入力したらスペース入れる
inoremap <expr>, smartchr#loop(', ', ',')

" ,の後にスペース入れてほしくないもの
autocmd Filetype vim inoremap <expr>, smartchr#loop(',', ', ')

" =でもスペース入れる
autocmd Filetype ruby inoremap <expr>= smartchr#one_of(' = ', ' => ', '=~','=')
autocmd Filetype javascript,coffee inoremap <expr>= smartchr#one_of(' = ', '=')
autocmd Filetype slim inoremap <expr>= smartchr#one_of('= ', '=')

" slimのときに-でスペース入れる
autocmd Filetype slim inoremap <expr>- smartchr#one_of('- ', '-')

" coffeeのときに-で->を作る
autocmd Filetype coffee inoremap <expr>- smartchr#one_of('-', '->')

" :でもスペース入れる
autocmd Filetype css,scss inoremap <expr>: smartchr#one_of(': ',  ':')

" +を+=と行き来
" autocmd Filetype ruby inoremap <expr>+ smartchr#loop(' + ',  ' += ', '+', '+=')
" autocmd Filetype javascript inoremap <expr>+ smartchr#loop(' + ',  '++',  ' += ', '+',  ' ++ ', '+=')
" autocmd Filetype ruby inoremap <expr>- smartchr#loop(' - ',  ' -= ',  '-',  '-=')
" autocmd Filetype javascript inoremap <expr>- smartchr#loop(' - ',  '--',  ' -= ', '-',  ' -- ')
" autocmd Filetype ruby,javascript inoremap <expr>* smartchr#loop(' * ',  ' *= ',  '*',  '*=')

" htmlとか、イコールにくっついてほしいものは明示
autocmd Filetype html,haml inoremap <expr>= smartchr#one_of('=',  ' = ')

"---------------------------------------------------------------------
" for emmet
"---------------------------------------------------------------------

let g:user_emmet_settings = {
      \   'html' : {
      \     'filters' : 'html',
      \    },
      \   'css' : {
      \     'filters' : 'fc',
      \   },
      \   'scss' : {
      \     'extends' : 'css',
      \     'filters' : 'fc',
      \   },
      \   'erb' : {
      \     'extends' : 'html',
      \     'filters' : 'html',
      \   },
      \ }


"---------------------------------------------------------------------
" for tcomment_vim
"---------------------------------------------------------------------

" コメントをノーマルモードでは<C-c>でトグルする
nnoremap <C-c> :<C-u>TComment<CR>

" ヴィジュアルモードでは<C-t>でトグルする
" 書き方がわからんｗｗｗ
" vnoremap <C-t> gc

" デフォルトにないfiletypeのコメントを追加
if !exists('g:tcomment_types')
  let g:tcomment_types = {}
endif

let g:tcomment_types['rspec'] = '# %s'
let g:tcomment_types['scss'] = '// %s'
let g:tcomment_types['coffee'] = '# %s'
let g:tcomment_types['slim'] = '/! %s'

"---------------------------------------------------------------------
" for はてなブログ
"---------------------------------------------------------------------

" <space>hではてなの機能を使う
autocmd Filetype text nnoremap [hatena] nop
autocmd Filetype text nmap <Space>h [hatena]
autocmd Filetype text nnoremap [hatena]cr :HatebloCreate<CR>
autocmd Filetype text nnoremap [hatena]cd :HatebloCreateDraft<CR>
autocmd Filetype text nnoremap [hatena]l :HatebloList<CR>
autocmd Filetype text nnoremap [hatena]u :HatebloUpdate<CR>
autocmd Filetype text nnoremap [hatena]d :HatebloDelete<CR>

" はてな記法入力支援
autocmd Filetype text inoreabbrev bq >><CR><<<ESC>
autocmd Filetype text inoreabbrev pre >\|<CR>\|<<ESC>
autocmd Filetype text inoreabbrev synpre >\|\|<CR>\|\|<<ESC><Up><Left>


"---------------------------------------------------------------------
" for Scss
"---------------------------------------------------------------------

" scssと同じファイル名でcssにコンパイル
function s:compile_scss()
  !sass % %:r.css
endfunction

" scss保存時にcssにコンパイル
" <SID>はスクリプトスコープの関数を呼ぶのに必要
autocmd BufWritePost *.scss call <SID>compile_scss()


"---------------------------------------------------------------------
" for lightline
"---------------------------------------------------------------------

let g:lightline = {
      \ 'colorscheme': 'default',
      \ 'active': {
      \   'left': [ [ 'mode', 'paste' ],
      \             [ 'fugitive', 'readonly', 'filename', 'modified' ] ]
      \ },
      \ 'component_function': {
      \   'readonly': 'DisplayReadOnly',
      \   'modified': 'DisplayModified',
      \   'fugitive': 'DisplayBranchName',
      \   'filename': 'DisplayFileName'
      \ },
      \ 'separator': { 'left': '>', 'right': '<' },
      \ 'subseparator': { 'left': '>', 'right': '<'}
      \ }

" readonlyの表示
function! DisplayReadOnly()
  if &filetype == "help"
    return ""
  elseif &readonly
    return "RO"
  else
    return ""
  endif
endfunction

" modifiedの表示
function! DisplayModified()
  if &filetype == "help"
    return ""
  elseif &modified
    return "+"
  elseif &modifiable
    return ""
  else
    return ""
  endif
endfunction

" gitのbranch名の表示
function! DisplayBranchName()
  if exists("*fugitive#head")
    let _ = fugitive#head()
    return strlen(_) ? '*'._ : ''
  endif

  return ''
endfunction

" ファイル名の表示
function! DisplayFileName()
  return  ('' != DisplayReadOnly() ? DisplayReadOnly() . ' ' : '') .
        \ ('' != expand('%:t') ? expand('%:t') : '[No Name]') .
        \ ('' != DisplayModified() ? ' ' . DisplayModified() : '')
endfunction
